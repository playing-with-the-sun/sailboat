def waituntilsupercapempty():
    pass
def waituntilsupercapfull():
    while Energy() <= 14:
        Plot(Energy())
        basic.pause(100)
def Energy():
    return min(Math.round(pins.analog_read_pin(AnalogPin.P2) / 30), 25)
def Plot(num: number):
    global X, Y
    basic.show_leds("""
        . . . . .
        . . . . .
        . . . . .
        . . . . .
        . . . . .
        """)
    X = 0
    Y = 0
    for index in range(5):
        for index2 in range(5):
            if num > 5 * X + Y:
                led.plot(X, 4 - Y)
                Y += 1
        X += 1
        Y = 0
def Motoroff():
    pins.digital_write_pin(DigitalPin.P8, 0)
def Motoron():
    pins.digital_write_pin(DigitalPin.P8, 1)
Y = 0
X = 0
bluetooth.start_io_pin_service()
bluetooth.start_led_service()

def on_forever():
    Motoroff()
    waituntilsupercapfull()
    Motoron()
    basic.pause(500)
    waituntilsupercapempty()
basic.forever(on_forever)
